// 查询用户的所有数据
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {_id}=event
  let res=await db.collection("User").doc(_id).get();
  return res;
};