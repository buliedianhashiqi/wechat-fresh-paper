// 修改购物车数据中的其他信息
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {_id,OrderInformation}=event;
  let res=await db.collection("User").doc(_id).update({
    OrderInformation:OrderInformation   
  });
  return res;
};