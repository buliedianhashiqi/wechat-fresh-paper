// 获得详细的热门推荐数据
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {sid}=event
  let res=await db.collection("hotPanel").where({
    sid:sid
  }).get();
  return res;
};
