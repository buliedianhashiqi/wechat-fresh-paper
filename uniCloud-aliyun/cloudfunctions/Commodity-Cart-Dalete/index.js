// 在修改商品模块中-根据传递过来的id删除对应的商品
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {id}=event
  let res=await db.collection("Classification").doc(id).remove();
  return res;
};