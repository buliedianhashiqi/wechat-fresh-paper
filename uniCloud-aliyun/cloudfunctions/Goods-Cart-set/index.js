// 查询用户数据表中的购物车商品
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {_id}=event;
  let res=await db.collection("User").where({
    _id:_id
  }).get();
  return res;
};