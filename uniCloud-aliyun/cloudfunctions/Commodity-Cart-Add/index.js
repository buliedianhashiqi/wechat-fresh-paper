// 在上架商品模块中添加商品
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {ShoppingData}=event
  let res=await db.collection("Classification").add({
    ...ShoppingData
  });
  return res;
};