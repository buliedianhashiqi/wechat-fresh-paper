// 在修改个人信息模块-修改用户表中的个人信息
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {id,data}=event
  const {sex,birthday,introduce,imageCover}=data
  const information={
        "sex": sex,
        "birthday": birthday,
        "introduce": introduce,
        "imageCover": imageCover
    }
  // 修改其他信息
  let res=await db.collection("User").doc(id).update({
    information:information
  });
  return res;
};