// 根据id查询单个商品
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {_id}=event
  let res=await db.collection("Classification").doc(_id).get();
  return res;
};
