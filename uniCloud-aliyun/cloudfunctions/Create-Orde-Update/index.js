// 修改购物车页的商品选中状态
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {_id,OrderForGoods}=event;
  let res=await db.collection("User").doc(_id).update({
    OrderForGoods:OrderForGoods   
  });
  return res;
};