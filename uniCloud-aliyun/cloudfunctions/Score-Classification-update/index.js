// 在评价页面添加/修改评价内容
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {sid,comment}=event
  // 添加评论内容
  let res=await db.collection("Classification").doc(sid).update({
    comment:comment
  });
  return res;
};