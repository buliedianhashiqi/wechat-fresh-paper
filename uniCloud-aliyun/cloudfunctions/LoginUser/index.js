// 用于获取和用户输入的账号信息
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {account}=event;
  let res=await db.collection("User").where({account:account}).get();
  return res;
};
