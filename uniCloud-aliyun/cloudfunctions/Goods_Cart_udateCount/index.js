// 修改商品数量
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {_id,countArr}=event;
  let res=await db.collection("User").doc(_id).update({
    ShoppingCart:countArr   
  });
  return res;
};