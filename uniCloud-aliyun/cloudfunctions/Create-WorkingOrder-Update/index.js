// 修改用户的暂存区的商品
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {_id,countArr}=event;
  let res=await db.collection("User").doc(_id).update({
    WorkingOrder:countArr   
  });
  return res;
};