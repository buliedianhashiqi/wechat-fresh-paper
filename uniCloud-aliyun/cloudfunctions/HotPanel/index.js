// 获取热门推荐表的所有数据
const db=uniCloud.database()
exports.main = async (event, context) => {
  // 连接数据表并进行操作
  let res=await db.collection("hotPanel").get();
  return res;
};
