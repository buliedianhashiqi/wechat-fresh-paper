// 根据点击的热门推荐的sid来展示不同的商品
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {sid}=event;
  // 连接数据表并进行操作
  let res=await db.collection("Classification").where({
    sid:sid
  }).get();
  return res;
};