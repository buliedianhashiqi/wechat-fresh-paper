// 用于获取被点击的那一类的上商品分类
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {cate}=event;
  // 连接数据表并进行操作
  let res=await db.collection("Classification").where({
    cate:cate
  }).field({"name":true,"price":true,"imgurl":true,"described":true}).get();
  return res;
};