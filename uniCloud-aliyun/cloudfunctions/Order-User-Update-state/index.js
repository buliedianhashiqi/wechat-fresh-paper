// 修改全部订单页订单的状态
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {id,OrderForGoods}=event
  // 修改收藏夹
  let res=await db.collection("User").doc(id).update({
    OrderForGoods:OrderForGoods
  });
  return res;
};