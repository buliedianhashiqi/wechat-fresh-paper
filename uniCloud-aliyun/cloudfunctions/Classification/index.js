// 查询所有和当前被点击的某一类相同的商品
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {name}=event;
  // 连接数据表并进行操作
  let res=await db.collection("Classification").where({
    kind:name
  }).get();
  return res;
};