// 在修改商品模块中-根据传递过来的参数修改数据库中的值
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {id,title,price,kind,cate,described}=event
  let res=await db.collection("Classification").doc(id).update({
    title:title,
    price:price,
    kind:kind,
    cate:cate,
    described:described
  });
  return res;
};