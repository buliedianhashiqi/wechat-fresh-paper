// 注册用户
const db=uniCloud.database()
exports.main = async (event, context) => {
  let {UserData}=event
  let res=await db.collection("User").add({
    ...UserData
  });
  return res;
};