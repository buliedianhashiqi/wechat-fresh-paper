// 在商品详情页修改我的页面的收藏夹内容
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {id,favorite}=event
  // 修改收藏夹
  let res=await db.collection("User").doc(id).update({
    favorite:favorite
  });
  return res;
};