// 在修改个人信息模块中查询用户表中的个人信息
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {_id}=event
  let res=await db.collection("User").doc(_id).get();
  return res;
};