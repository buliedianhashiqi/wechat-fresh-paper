// 在修改商品模块中-根据 id 来搜索商品
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {_id}=event
  let res=await db.collection("Classification").doc(_id).get();
  return res;
};
