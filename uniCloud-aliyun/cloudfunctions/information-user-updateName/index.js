// 在修改个人信息模块-修改用户表中的个人信息中的id
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {id,name}=event
  // 修改名称
  let res=await db.collection("User").doc(id).update({
    userName:name
  });
  return res;
};