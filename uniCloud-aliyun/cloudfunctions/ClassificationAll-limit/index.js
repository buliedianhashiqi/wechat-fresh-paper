// 查询所有的商品直接展示用
const db=uniCloud.database()
exports.main = async (event, context) => {
  // 翻页功能
  let {limit=6} = event
  let res=await db.collection("Classification").limit(limit).get();
  return res;
};