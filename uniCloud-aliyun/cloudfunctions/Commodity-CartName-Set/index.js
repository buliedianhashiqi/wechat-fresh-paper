// 在修改商品模块中-根据 name 来搜索商品
const db=uniCloud.database()
exports.main = async (event, context) => {
  const {name}=event
  let res=await db.collection("Classification").where({
    name:name
  }).get();
  return res;
};